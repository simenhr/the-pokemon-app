import { Component, OnInit } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon/pokemon.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pokemon-catalogue-list',
  templateUrl: './pokemon-catalogue-list.component.html',
  styleUrls: ['./pokemon-catalogue-list.component.css']
})
export class PokemonCatalogueListComponent implements OnInit {

  public pokemons: any = [];
  public counter: number = 0;

  constructor(private pokemonService: PokemonService, private router: Router) { }

  ngOnInit(): void {
    this.getAllPokemon(151);
  }

  async getAllPokemon(numberOfPokemonsWanted: number) {
    for (let i = 1; i < numberOfPokemonsWanted + 1; i++) {
      try {
        this.pokemons.push(await this.pokemonService.getPokemon(i));
        this.pokemons[this.counter].name = this.pokemons[this.counter].name.charAt(0).toUpperCase() + this.pokemons[this.counter].name.slice(1);
        this.counter++;
      } catch (error) {
        console.error(error.message);
      }

    }
  }

  onPokemonClicked(pokemonId: number) {
    this.router.navigate(['/pokemon-catalogue', pokemonId])
  }

}
