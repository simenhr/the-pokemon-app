import {NgModule} from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { PokemonCatalogueListComponent } from "./pokemon-catalogue-list.component";
import { PokemonCatalogueListItemComponent } from "../pokemon-catalogue-list-item/pokemon-catalogue-list-item.component";
import { CommonModule } from '@angular/common';

const routes: Routes = [
    {
        path: '',
        component: PokemonCatalogueListComponent
    }
];

@NgModule({
    declarations:[PokemonCatalogueListComponent, PokemonCatalogueListItemComponent],
    imports: [RouterModule.forChild(routes), CommonModule],
    exports: [RouterModule]
})

export class PokemonCatalogueListModule {}