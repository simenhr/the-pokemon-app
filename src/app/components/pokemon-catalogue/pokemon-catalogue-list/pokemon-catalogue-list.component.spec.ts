import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonCatalogueListComponent } from './pokemon-catalogue-list.component';

describe('PokemonCatalogueListComponent', () => {
  let component: PokemonCatalogueListComponent;
  let fixture: ComponentFixture<PokemonCatalogueListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokemonCatalogueListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonCatalogueListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
