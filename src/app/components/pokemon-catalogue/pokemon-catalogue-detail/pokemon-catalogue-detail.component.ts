import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonService } from 'src/app/services/pokemon/pokemon.service';
import { Pokemon } from 'src/app/models/pokemon.model';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-pokemon-catalogue-detail',
  templateUrl: './pokemon-catalogue-detail.component.html',
  styleUrls: ['./pokemon-catalogue-detail.component.css']
})
export class PokemonCatalogueDetailComponent implements OnInit {

  @Output() clickCollect: EventEmitter<any> = new EventEmitter();

  public pokemon: any = null;

  constructor(private router: Router, private pokemonService: PokemonService, private session: SessionService) { }

  async ngOnInit() {
    try {
      this.pokemon = await this.pokemonService.getPokemon(parseInt(this.router.url.split('/').pop()));
      this.pokemon.name = this.pokemon.name.charAt(0).toUpperCase() + this.pokemon.name.slice(1);
      console.log(this.pokemon);
    } catch (error) {
      console.error(error.message);
    }
    
  }

  onCollectButtonClicked() {
    let {user, pokemon} = this.session.get();
    pokemon = [...pokemon, this.pokemon];
    this.session.save({user,
    pokemon});
    this.router.navigate(['/pokemon-catalogue'])
  }

  onGoBackButton() {
    this.router.navigate(['/pokemon-catalogue']);
  }

}
