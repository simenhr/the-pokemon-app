import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonCatalogueDetailComponent } from './pokemon-catalogue-detail.component';

describe('PokemonCatalogueDetailComponent', () => {
  let component: PokemonCatalogueDetailComponent;
  let fixture: ComponentFixture<PokemonCatalogueDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokemonCatalogueDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonCatalogueDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
