import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonCatalogueListItemComponent } from './pokemon-catalogue-list-item.component';

describe('PokemonCatalogueListItemComponent', () => {
  let component: PokemonCatalogueListItemComponent;
  let fixture: ComponentFixture<PokemonCatalogueListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokemonCatalogueListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonCatalogueListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
