import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pokemon-catalogue-list-item',
  templateUrl: './pokemon-catalogue-list-item.component.html',
  styleUrls: ['./pokemon-catalogue-list-item.component.css']
})
export class PokemonCatalogueListItemComponent implements OnInit {

  @Input() pokemon;
  @Output() clickPokemon: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onPokemonItemClicked() {
    this.clickPokemon.emit(this.pokemon.id);
  }

}
