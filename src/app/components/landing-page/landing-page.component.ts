import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { SessionService } from "../../services/session/session.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  registerTrainerForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(3)])
  });

  constructor(private session: SessionService, private router: Router) { 
    if (this.session.get() !== false) {
      this.router.navigateByUrl('/trainer-page');
    }
   }

  ngOnInit(): void {
  }

  get username() {
    return this.registerTrainerForm.get('username');
  }

  onLoginClicked() {
    try {
      this.session.save({user: this.registerTrainerForm.value.username, pokemon: []})
      this.router.navigateByUrl('trainer-page');
    } catch (error) {
      console.error(error.message);
    }
  }

}
