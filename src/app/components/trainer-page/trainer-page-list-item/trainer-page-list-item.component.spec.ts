import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainerPageListItemComponent } from './trainer-page-list-item.component';

describe('TrainerPageListItemComponent', () => {
  let component: TrainerPageListItemComponent;
  let fixture: ComponentFixture<TrainerPageListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainerPageListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainerPageListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
