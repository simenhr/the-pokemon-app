import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-trainer-page-list-item',
  templateUrl: './trainer-page-list-item.component.html',
  styleUrls: ['./trainer-page-list-item.component.css']
})
export class TrainerPageListItemComponent {

  @Input() pokemon;
  @Output() clickPokemon: EventEmitter<number> = new EventEmitter();

  constructor() { }

  onPokemonItemClicked() {
    this.clickPokemon.emit(this.pokemon.id);
  }
  

}
