import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainerPageListComponent } from './trainer-page-list.component';

describe('TrainerPageListComponent', () => {
  let component: TrainerPageListComponent;
  let fixture: ComponentFixture<TrainerPageListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainerPageListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainerPageListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
