import {NgModule} from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { TrainerPageListComponent } from "./trainer-page-list.component";
import { TrainerPageListItemComponent } from "../trainer-page-list-item/trainer-page-list-item.component";
import { CommonModule } from '@angular/common';

const routes: Routes = [
    {
        path: '',
        component: TrainerPageListComponent
    }
];

@NgModule({
    declarations:[TrainerPageListComponent, TrainerPageListItemComponent],
    imports: [RouterModule.forChild(routes), CommonModule],
    exports: [RouterModule]
})

export class TrainerPageListModule {}