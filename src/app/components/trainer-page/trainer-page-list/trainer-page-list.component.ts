import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session/session.service';
import { PokemonService } from "../../../services/pokemon/pokemon.service";
import { Pokemon } from "../../../models/pokemon.model";
import { Router } from '@angular/router';

@Component({
  selector: 'app-trainer-page-list',
  templateUrl: './trainer-page-list.component.html',
  styleUrls: ['./trainer-page-list.component.css']
})
export class TrainerPageListComponent implements OnInit {

  public pokemons: any[] = this.session.get().pokemon;
  public username: string = this.session.get().user;

  constructor(private session: SessionService, private pokemonService: PokemonService, private router: Router) { }

  ngOnInit() {
  //  for (let i = 0; i < this.pokemons.length; i++) {
  //    this.pokemons[i] = this.setCapitalLetterForPokemonName(this.pokemons[i]);
  //  }

  }

  public setCapitalLetterForPokemonName(pokemonName: string) {
    pokemonName = pokemonName.charAt(0).toUpperCase() + pokemonName.slice(1);

  }

  public onPokemonClicked(pokemonId: number) {
    this.router.navigate(['pokemon-catalogue', pokemonId])
  }

  public onLogOutClicked() {
    if (confirm('Are you sure? All your pokemon will be lost!')) {
      this.session.logout();
      this.router.navigateByUrl('/landing-page');
    }
  }

}
