export interface Pokemon {
    name: string,
    sprites: any,
    stats: any,
    types: any
}