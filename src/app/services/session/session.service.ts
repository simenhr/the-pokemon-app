import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }

  save(session: any) {
    localStorage.setItem('user_session', JSON.stringify(session));
  }

  get() {
    const savedSession = localStorage.getItem('user_session');
    return savedSession ? JSON.parse(savedSession) : false;
  }

  logout() {
    localStorage.clear();
  }
}
