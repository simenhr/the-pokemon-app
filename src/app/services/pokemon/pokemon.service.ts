import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { Pokemon } from 'src/app/models/pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private http: HttpClient) { }

  getPokemon(pokemonId: number): Promise<any> {
    return this.http.get<Pokemon>(`${environment.apiUrl}/pokemon/${pokemonId}`)
    .toPromise();
  }
}
