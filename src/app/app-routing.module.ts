import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { AuthGuard } from "./guards/auth.guard";
import { PokemonCatalogueDetailComponent } from "./components/pokemon-catalogue/pokemon-catalogue-detail/pokemon-catalogue-detail.component";
import { PageNotFoundComponent } from "./components/page-not-found/page-not-found.component";

const routes: Routes = [
  {
    path: 'landing-page',
    component: LandingPageComponent
  },
  {
    path: 'trainer-page',
    loadChildren: () => import('./components/trainer-page/trainer-page-list/trainer-page-list.module').then(m => m.TrainerPageListModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'pokemon-catalogue',
    loadChildren: () => import('./components/pokemon-catalogue/pokemon-catalogue-list/pokemon-catalogue-list.module').then(m => m.PokemonCatalogueListModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'pokemon-catalogue/:id',
    component: PokemonCatalogueDetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/landing-page'
  },
  {
    path: '**',
    component: PageNotFoundComponent 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
